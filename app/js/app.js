'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ui.bootstrap',
  'ngRoute',
  'ngGrid',
  'ngDragDrop',
  'googlechart',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.mainController',
  'myApp.invoiceDetail',
]).
config(['$routeProvider', '$httpProvider', 'requestNotificationProvider', function($routeProvider,$httpProvider,requestNotificationProvider) {
  $routeProvider.when('/main', {templateUrl: 'partials/main.html', controller: 'mainController'});
  $routeProvider.when('/invoice_detail/:id', {templateUrl: 'partials/invoice_detail.html', controller: 'invoiceDetail'});
  
  $routeProvider.otherwise({redirectTo: '/main'});

}]);



// CREATE AND SYNC BD //
var db = new PouchDB('http://admin:18041981@socialinsight.iriscouch.com/invoices');
var localDb = new PouchDB('invoices', {adapter : 'websql'});

function sync() {
  var opts = {live: true};
  localDb.replicate.from(db, opts);
  localDb.replicate.to(db, opts);
}

sync();
// CREATE AND SYNC BD //






var SETTINGS = {};
    SETTINGS.personal_info = {};
    SETTINGS.currency                = "€"
    SETTINGS.inps                    = 4;
    SETTINGS.iva                     = 22;
    SETTINGS.ritenuta                = 20;
    SETTINGS.personal_info.name      = 'Cesare Malescia';
    SETTINGS.personal_info.address   = 'Via de Angelis, 14 - 84012 Angri (SA)';
    SETTINGS.personal_info.tel       = '+39 328 4172724';
    SETTINGS.personal_info.mail      = 'cesare.malescia@gmail.com';
    SETTINGS.personal_info.piva      = '04542080652';
    SETTINGS.personal_info.cf        = 'MLSCSR81D18F912V';

    
