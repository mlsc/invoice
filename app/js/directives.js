'use strict';

/* Directives */


angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);


/*
angular.module('myApp.directives', [])
    .directive('loadingWidget', function (requestNotification, $rootScope) {
    $rootScope.loading = [];
    $rootScope.loading.count = requestNotification.getRequestCount;
    $rootScope.loading.jsLoading = 0;

    
    
    return {
        restrict: "AC",
        link: function (scope, element) {
            // hide the element initially
            $rootScope.loading.show='none';

            //subscribe to listen when a request starts
            requestNotification.subscribeOnRequestStarted(function () {
                // show the spinner!
                $rootScope.loading.show='block';
            });

            requestNotification.subscribeOnRequestEnded(function () {
                // hide the spinner if there are no more pending requests
                if (requestNotification.getRequestCount() === 0) $rootScope.loading.show='none';
            });


            $rootScope.loading.toggleLoading = function(display){
                if(display === 'show'){
                    $rootScope.loading.jsLoading = $rootScope.loading.jsLoading+1;
                    $rootScope.loading.show='block';
                }
                if(display === 'hide'){
                    $rootScope.loading.jsLoading = $rootScope.loading.jsLoading-1;
                    if($rootScope.loading.jsLoading <= 0) $rootScope.loading.show='none';
                }
            }

           
        }
    };
});*/