'use strict';


angular.module('myApp.mainController', [])
  .controller('mainController', ['$scope', '$rootScope', '$location', '$http',  function($scope, $rootScope, $location, $http) {

  
    
    $rootScope.settings = SETTINGS;
    $rootScope.personalInfo = SETTINGS.personal_info;

    $scope.invoices = [];

    $scope.isLoadingDb = false;


    



    var loadInvoices = function(){
      var map = function(doc){
        emit(doc.number,doc);
      }
      localDb.query({map:map},{include_docs: true}).catch(function(error){
        console.log('error');
        console.log(error);
        return;
      }).then(function(response){
        console.log('Invoices LOADED from db');
        // console.log(response);
        $scope.$apply(function() { 
          $scope.invoices = response.rows;
          $scope.isLoadingDb = false;
        });
      });

    }

    //SYNC
    localDb.changes({since: 'now', live: 'true'}).on('change', loadInvoices);
    //SYNC



    




    $scope.invoicesArchive = { 
      data: 'invoices',
      columnDefs: [
        {displayName: 'Numero', cellTemplate:'<div>{{row.entity.doc.number}}/a/{{row.entity.doc.month}}</div>'},
        {field:'doc.customer.name', displayName:'Cliente'},
        {field:'doc.date', displayName:'Data'},
        {field:'doc.total', displayName:'Totale Fattura'},
        {displayName:'actions',cellTemplate:'<button ng-click="view(row.entity)">view</button><button ng-click="create(row.entity)">duplicate</button>'},
        {displayName:'delete',cellTemplate:'<button ng-click="delete(row.entity)">delete</button>'}
      ]
    };


    $scope.view = function(invoice){
      console.log("VIEW");
      console.log(invoice);
      $location.path('/invoice_detail/'+invoice.id);
    }    

    $scope.delete = function(invoice){
      localDb.remove(invoice.doc).catch(function(error){
        console.log('error');
        console.log(error);
        return;
      }).then(function(response){
        console.log('invoice n'+ invoice.id +' DELETED from db');
        loadInvoices();
      });
    }


    var getCurrentMont = function(){
      return ["GEN","FEB","MAR","APR","MAG","GIU","LUG","AGO","SET","OTT","NOV","DIC"][new Date().getMonth()];
    }

    var newInvoice = function(invoice){
      var items;
      var customer;
      var total;

      if(!invoice){

        items = [{
          description         : 'descrizione',
          unit_cost           : '0',
          qty                 : '1',
          note                : 'note'
        }];
        customer = {
          name            : 'nome cliente',
          address         : 'indirizzo cliente',
          piva            : '000000000000000',
          cf              : '000000000000000',
          note            : 'lorem ipsum',
          contactPerson   : 'persona',
        };
        total = 0;

      }else{
        // console.log(invoice);
        items = invoice.doc.items;
        customer = invoice.doc.customer;
        total = invoice.doc.total;
      }
      
      if($scope.invoices.length==0)
        var invoiceNumber = '1';
      else
        var invoiceNumber = (Number($scope.invoices[$scope.invoices.length-1].doc.number)+1).toString();
      
      var invoice = {
        number:invoiceNumber,
        month: getCurrentMont(),
        customer:customer,
        date: new Date().toLocaleDateString(),
        items:items,
        total:total,
      };

      createInvoice(invoice)
    }



    var createInvoice = function(invoice){
      $scope.isLoadingDb = true;
      console.log(invoice);
      localDb.post(invoice)
        .catch(function(error){
          console.log(error)
        })
        .then(function(response){
          console.log(response);
          loadInvoices();
        });
    }
   
    $scope.create = function(invoice){
      newInvoice(invoice);
    } 

    loadInvoices();
   
    
  }]);






  



