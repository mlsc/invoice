'use strict';


angular.module('myApp.invoiceDetail', [])
  .controller('invoiceDetail', ['$scope', '$rootScope', '$location', '$http', '$routeParams', function($scope, $rootScope, $location, $http, $routeParams) {

    console.log($routeParams);


    $rootScope.settings = SETTINGS;
    $rootScope.personalInfo = SETTINGS.personal_info;

    $scope.editing = false;
    $scope.isNewInvoice = false;

    
    var invoiceId = $routeParams.id;

    $scope.invoice = {};
    
    
    var loadInvoice = function(id) {
      localDb.get(id).catch(function(error){
        console.log('fattura non trovata');
        // console.log(error);
        $location.path('/main');
      }).then(function(response){
        console.log('invoice n'+ id +' loaded from db');
        $scope.$apply(function() { 
          $scope.invoice = response;
          $rootScope.title = "fattura_" + response.number + "_" + response.month;
        });
      });
    }

    loadInvoice(invoiceId);

    






    $scope.invoice_sub_total = function() {
        var total = 0.00;
        angular.forEach($scope.invoice.items, function(item, key){
          total += (item.qty * item.unit_cost);
        });
        return total;
    }

    $scope.inps = function() {
        return Math.ceil((($rootScope.settings.inps * $scope.invoice_sub_total())/100)*100)/100;
    }

    $scope.imponibile = function(){
      return Math.ceil(($scope.invoice_sub_total() + $scope.inps())*100)/100;
    }

    $scope.iva = function(){
      return Math.ceil((($rootScope.settings.iva * ($scope.invoice_sub_total()+$scope.inps()))/100)*100)/100; 
    }

    $scope.total = function(){
      return Math.ceil(($scope.invoice_sub_total() + $scope.inps() + $scope.iva())*100)/100;
    }

    $scope.ritenuta = function(){
      return Math.ceil((($rootScope.settings.ritenuta * $scope.imponibile())/100)*100)/100; 
    }

    $scope.net = function(){
      return Math.ceil(($scope.total() - $scope.ritenuta())*100)/100;
    }




    $scope.editInvoice = function(){
      $scope.editing = true;
    }





    $scope.addItem = function(){
      console.log('add Item');

      var item = {
        description         : 'descrizione',
        unit_cost           : '0',
        qty                 : '1',
        note                : 'note'
      };
      $scope.invoice.items.push(item);
    }


    $scope.deleteItem = function(item){
      console.log('delete Item');
      console.log(item);
      var index = $scope.invoice.items.indexOf(item);
      if (index != -1) {
        $scope.invoice.items.splice(index, 1);
      }
    }


    $scope.goBack = function(){
      $location.path('/main');
    }


    $scope.printInvoice = function(){
      window.print();
    }






    $scope.updateInvoice = function(){
      $scope.invoice.total = $scope.net();
      localDb.put($scope.invoice, $scope.invoice._id, $scope.invoice._rev).catch(function(error){
        console.log('error');
        console.log(error);
        return;
      }).then(function(response){
        console.log('invoice n'+ $scope.invoice._id +' UPDATED from db');
        loadInvoice($scope.invoice._id);
      });

      $scope.editing = false; 
    }







  }]);






  



